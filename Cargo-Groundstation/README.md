# Cargo Application Information

## TABLET SETUP
  * Connect to wifi
  * Download File Manager, DJI GO, qGroundControl, Precision GPS, Termius, Net Analyser
  * Enable application form unknown sources, depending on Android version :
    * Old versions : Open Settings->Lock screen and security
	  * Active the option : "Unknwon sources"
    * New versions : Open Settings->Application and Notifications->Specific access of applications->Unknown source
	  * Allow the "File Manager" to install application from unknown sources
  * Connect to computer with a USB cable, allow file USB transfer connection (option available in the notifications)
    * Copy the APK file and the Cargo_GS folder in the tablet Main Storage
  * Open the file manager and navigate to the Main Storage
  * Install the Cargo GroundStation
  * Update the configuration file of the Cargo Groundstation
  * The app is now opperational

## NECESSARY FILES

### CONFIG FILE
A json config file named cargo_GS_config.json needs to be placed in the tablet internal storage in Internal shared storage\Cargo_GS.<br/>
It contains the following information:<br/>

```json
{
    "drones": [
        {
            "address": "0013A2004193CB48",
            "id": 50,
            "name": "M600 A"
        }
    ],
    "gsIp": "192.168.0.87",
    "gsTcpPort": "12345",
    "receivingPilotName": "Receiver",
    "relays": [
        {
            "address": "0013A20041887882",
            "id": 151,
            "latitude": 28.128195,
            "longitude": 82.874555,
            "name": "Relay A"
        },
        {
            "address": "FF00FF00FF00FF00",
            "id": 152,
            "latitude": 28.075061,
            "longitude": 82.839317,
            "name": "Relay B"
        },
        {
            "address": "FF00FF00FF00FF00",
            "id": 153,
            "latitude": 28.116112,
            "longitude": 82.834716,
            "name": "Relay C"
        }
    ],
    "sendingPilotName": "Sender",
    "serialBaudrate": 115200,
    "stations": [
        {
            "address": "0013A2004188786E",
            "id": 101,
            "latitude": 28.103866962190665,
            "local": false,
            "longitude": 82.85372614771416,
            "name": "Pyuthan Hospital"
        },
        {
            "address": "FF00FF00FF00FF00",
            "id": 102,
            "latitude": 28.099150567937354,
            "local": true,
            "longitude": 82.85030413654863,
            "name": "Aayushma"
        },
        {
            "address": "FF00FF00FF00FF00",
            "id": 103,
            "latitude": 28.13145123424568,
            "local": false,
            "longitude": 82.8947422191747,
            "name": "Dharmawati"
        },
        {
            "address": "FF00FF00FF00FF00",
            "id": 104,
            "latitude": 28.106591567725562,
            "local": false,
            "longitude": 82.81636375331902,
            "name": "Addapata"
        },
        {
            "address": "FF00FF00FF00FF00",
            "id": 105,
            "latitude": 28.10387322196331,
            "local": false,
            "longitude": 82.8537390000995,
            "name": "Majhakot"
        }
    ],
    "undefined": [
        {
            "id": 200,
            "name": "Webserver 1"
        }
    ],
    "useConnectionApi": true,
    "useDjiMissions": false,
    "webserverIp": "51.75.168.103",
    "webserverTcpPort": "9929"
}
```
The local station id needs to be adapted for each tablet. Relays and stations information need to be adapted depending on the system setup, as well as connection and pilot information. The DJImission parameter needs to be false to use the waypoint commands allowing to go below the take-off altitude.

### MISSION FILES
Mission files are .plan files created using qGroundControl. They need to be named using the following format:<br/>
```
id_version_name-without-underscore.plan
Example: 4_1_Bohechio-ElCoco.plan
```
They need to be placed in every tablet's internal storage in ```Internal shared storage\Cargo_GS\missions``` as well as on the drone extension.
