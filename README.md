# M600 Cargo UP - Software & Documentation repository

This repository is intended to provide to the M600 Caro UP user's the last version of the software as well as the documentation to setup the complete system.

## Software and documentation
The software and associated documentation are separated into the folowing catergories. On this repostitory a subfolder contains the detail, files and executable for each category.

* Onboard application : The application running inside the cargo extension. It is related to the hardware physically connected to the drone.
* Cargo Groundstation : This is the application running on the tablets. It features, the SOPs, mission setup and mission monitoring.
* Remote server : The remote server is a required software for LTE (3G/4G) connection.
* Mission planing : A short documentation regarding the guidelines to plan a mission.

## System overview

![System overview](system_overview.png)
