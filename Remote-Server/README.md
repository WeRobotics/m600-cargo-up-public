# Remote server

## Requirement

The remote server application should be running on a server with a public IP address. We recommand a server running the last LTS version of Ubuntu. You should have administrator rights on the server.

## Installation procedure

1. Connect to the server trough SSH
2. Update and upgrade your server :
	1. ```sudo apt update```
	2. ```sudo apt upgrade```
3. Copy the "mavlink_router" and the "cargo_mavlink_router.service" files to the server. Multiple options to do this. (At the end the files should be located in "~/m600-crago-up-public/Remote-Server/")
    1. ```cd ~/```
	2. ```sudo apt install git```
	3. ```git clone https://gitlab.com/WeRobotics/m600-cargo-up-public```
	4. The last command will ask you to type your gitlab credentials.
	5. You now have a version of the two files in : "~/m600-cargo-up-public/Remote-Server/"
	6. To run the mavlink_router you need to make it executable: ```sudo chmod u+x ~/m600-crago-up-public/Remote-Server/mavlink_router```
4. Install the service to run the mavlink router
	1. If your username is not "ubuntu" you should modify the "User" and "ExecStart" fields in the "cargo_mavlink_router.service". You can do this using nano :
		1. ```nano ~/m600-cargo-up-public/Remote-Server/cargo_mavlink_router.service```
		2. Set the "User" field to your current account name.
		3. Press "CTRL+X" followed by "Y" and finally press "ENTER" to save your modifications
	2. Copy the service file : ```sudo cp ~/m600-cargo-up-public/Remote-Server/cargo_mavlink_router.service /etc/systemd/system/```
	3. Active the service at boot : ```sudo systemctl enable cargo_mavlink_router```
	4. Reload the services : ```sudo systemctl daemon-reload```
	5. Start the service : ```sudo systemctl start cargo_mavlink_router```
5. The mavlink router should now be installed and running on your server you can check it's status with : ```sudo systemctl status cargo_mavlink_router```
6. Update the config file on the CargoUp and the Tablets to match IP (e.g. 0.0.0.0) of your server and the ports of the remote server application.
	* CargoUP: edit config file line 17 and 18 with ```nano ~/cargo-onboard/cargo-onboard/build/bin/config.yaml```
		* ```ip        : 0.0.0.0```
		* ```port      : 9930```
	* Tablets: edit line 8 and 9 in file ```Internal shared storage/Cargo_GS/cargo_GS_config.json```
		* ```"webserverIp": "0.0.0.0",```
        * ```"webserverTcpPort": "9929",```

