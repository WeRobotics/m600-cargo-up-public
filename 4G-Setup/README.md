# 4G connection setup

## Prerequisites
  * Buy a SIM card (micro SIM form-factor) from a cellular provider that covers the area you intend to fly.
  * Prepare SIM card.
    * Insert SIM card into a phone and disable the SIM card lock. 
      * Android: ```Settings > Security > Set up SIM card lock > disable "Lock SIM card"```
      * iPhone: ```Settings > Cellular > SIM-PIN > disable "SIM-PIN"```
    * Restart your phone and make sure it is not asking you for the SIM PIN.
  * Find the Access Point Name (APN) of your cellular provider and note it for later.
    * Android: ```Settings > Connections >  Mobile networks > Access Point Names```
    * iPhone: ```Settings > Cellular > Cellular Data Options > Cellular Network > APN```

## Insert SIM card
 * Open the M600 CargoUP.
 * Insert SIM card as follows:
![insert_sim](4G-Setup/insert_sim_card.jpg)

## Set APN on CargoUP
 * Connect to CargoUP over SSH with your preferred tool.
 * Set the APN in the config file of the 4G module.
   * Open config file
     * ```sudo nano /etc/ppp/peers/vzw-QBG96-chat```
   * On line 37 replace ```gprs.swisscom.ch``` with the APN you noted earlier.
     * ```OK AT+CGDCONT=3,"IPV4V6","gprs.swisscom.ch"```
   * Press "CTRL+X" followed by "Y" and finally press "ENTER" to save your modifications
   * Reboot the system
     * ```sudo reboot```
