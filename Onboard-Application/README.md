# Cargo onboard configuration

## Configure the CargoUP
 * Connect to remote server over SSH with your preferred tool
 * Get the public IP address from your server
   * ```ifconfig```
   * The address should be an IPv6 address
 * Connect to CargoUP over SSH with your preferred tool
 * Configure the remote server IP address
   * Open config file
     * ```nano /home/pi/cargo-onboard/cargo-onboard/build/bin/config.yaml```
   * On line 17 replace the IP address with the one from your server
   * Press "CTRL+X" followed by "Y" and finally press "ENTER" to save your modifications
   * Restart the cargo application
     * ```sudo service wer-cargo-onboard restart```

## Precision landing
The precision landing feature rely on the visual detection of Aruco tags. Therefore, the precision landing feature can not be used if the tag are not well illuminated. Moreover, to avoid reflections, the tags should be made out of fabric. 

The Aruco tags are provided in the "tags" folder in svg format. Tags should be made at a specific size, the size is indicated in the tag filename. When executing a precision landing, the drone will align with the smaller detectable tag. The recommended pattern to organize teh tags is illustarted in "Landing tag pattern.pdf".

The bigger tag has a maximum detection range arround 50 meters.

## Camera offset

If during precision landings you are observing a slight descent angle, you can try to adjust the camera angle in the config file. Do this with caution as a wrong parameters could lead to unexpected behavior.

 * Connect to CargoUP over SSH with your preferred tool
 * Configure the camera angle
   * Open config file
     * ```nano /home/pi/cargo-onboard/cargo-onboard/build/bin/config.yaml```
   * On line 27 and 28 replace the camera angle. The angle should be provided in **radians**. The value can be negative and should not exceed 0.05 radians.
   * Press "CTRL+X" followed by "Y" and finally press "ENTER" to save your modifications
   * Restart the cargo application
     * ```sudo service wer-cargo-onboard restart```
